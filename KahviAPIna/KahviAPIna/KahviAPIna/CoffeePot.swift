//
//  CoffeePot.swift
//  KahviAPIna
//
//  Created by Saija Saarenpää on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

class CoffeePot: NSObject {
    var id: NSInteger = 0
    var name: String = ""
    var latest_level: CoffeeLevel?
    
    init(id: NSInteger, name: String, latest_level: CoffeeLevel?) {
        self.id = id
        self.name = name
        self.latest_level = latest_level
    }
}
