//
//  VINModel.h
//  ios-utilslib
//
//  Created by Sami Koskimäki on 10/3/13.
//  Copyright (c) 2013 Vincit Oy. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class that provides automatic serialization of properties to (and parsing from) Foundation objects.
 *
 * There are also helper methods for serializing to (and parsing from) JSON strings, NSData and files.
 *
 * Usage example:
 *
 *
 * @code{.m}
 *
 * @interface MyModel : VINModel
 *
 * @property (nonatomic, strong) NSArray *foo;
 * @property (nonatomic, strong) NSString *bar;
 * @property (nonatomic, strong) NSNumber *baz;
 * @property (nonatomic, assign) int spam;
 * @property (nonatomic, assign) MyModel *eggs;
 *
 * @end
 *
 * @implementation MyModel
 *
 * + (NSDictionary *) explicitObjectMappings {
 *
 *     return @{
 *         @"foo" : [MyModel class],
 *     };
 * }
 *
 * @end
 *
 * @endcode
 *
 *
 * Serializing an object of class MyModel (by calling [obj serialize]) would create a dictionary like this:
 * (assuming there is one object in the 'foo' array)
 *
 *
 * @code{.m}
 *
 * NSDictionary *serialized = @{
 *     @"foo" : @[
 *         @{
 *             @"foo"  : @[],
 *             @"bar"  : @"some string",
 *             @"baz"  : @3.14,
 *             @"spam" : @42,
 *             @"eggs" : null
 *         }
 *     ],
 *     @bar  : @"some string 2",
 *     @baz  : @1.57,
 *     @spam : @5,
 *     @eggs : @{
 *         @"foo"  : @[],
 *         @"bar"  : @"some string 3",
 *         @"baz"  : @6.28,
 *         @"spam" : @1,
 *         @"eggs" : null
 *     }
 * };
 *
 * @endcode
 *
 *
 * All properties except collections are mapped automatically. Collection properties need
 * to be explicitly mapped because the class of their content cannot be known at compile time.
 * See the documentation of explicitObjectMappings method for more information about explicit
 * mapping.
 */
@interface VINModel : NSObject <NSCoding, NSCopying>

/**
 * Extra information about mappings between properties and the serialized dictionary representation.
 *
 * There are three main cases where this method is useful:
 *
 * 1. Collection mapping:
 *
 *     For automatic parsing and serialization the types of the properties need to be known. For most properties the
 *     type is encoded in the definition, but for collections only the container class is known. This method must
 *     return the mappings between the collection properties and the classes the collections should contain.
 *
 * 2. Mapping a property to an object with different key:
 *
 *     We don't always want to have same name for the attribute and the corresponding item in the dictionary. This
 *     method can be used to map an attribute to an arbitrary dictionary item. For example the dictionary may contain
 *     @{@"foo" : @10} and you want the 'bar' property of your model to get the value of foo.
 *
 * 3. Omitting properties from serialization/parsing:
 *
 *     Your model classes may have properties you don't want to serialize. Using this method those properties
 *     can be easily excluded.
 *
 *
 * The keys of the returned dictionary must be property names and the values must have one of the
 * following types:
 *
 *
 * 1. Class object (the object returned by the static method "class" of each NSObject).
 *
 *    Indicates the class of the objects the property contains. Only needed for collections
 *    (NSArray, NSSet, NSDictionary).
 *
 * 2. NSString object.
 *
 *    Indicates the key of the item in the serialized dictionary where the property will be serialized to and parsed from.
 *
 * 3. NSNumber object @NO.
 *
 *    Indicates that the corresponding property should be excluded from the serialization/parsing process.
 *
 * 4. NSArray object.
 *
 *    The value can also be an array containing one or more of the value types 1, 2 or 3.
 *
 *
 * Example:
 *
 *
 * @code{.m}
 *
 * @interface MyModel : VINModel
 *
 * @property (nonatomic, assign) int a;
 * @property (nonatomic, strong) NSNumber *b;
 * @property (nonatomic, strong) MyModel *c;
 *
 * @property (nonatomic, strong) NSArray *foo;
 * @property (nonatomic, strong) NSString *bar;
 * @property (nonatomic, strong) NSSet *baz;
 *
 * @end
 *
 * @implementation MyModel
 *
 * + (NSDictionary *) explicitObjectMappings {
 *
 *     return @{
 *         @"foo" : [SubclassOfVINModel class],
 *         @"bar" : @NO,
 *         @"baz" : @[@"jeah", [SubclassOfVINModel class]]
 *     };
 * }
 *
 * @end
 *
 * @endcode
 *
 *
 * In this example all three use cases of explicitObjectMappings method are demonstrated. A collection
 * property foo is assumed to contain SubclassOfVINModel objects. bar property is omitted from the
 * serialization/parsing and baz property is given the value of @"jeah" in the serialized dictionary.
 *
 * The properties a, b and c are automatically mapped to the objects with the same keys. There is no
 * need to explicitly map them.
 *
 * @note This method is called only once when it is first used.
 */
+ (NSDictionary *) explicitObjectMappings;

/**
 * Parses a dictionary to a VINModel instance.
 *
 * @throw NSException
 *      If parsing failed. The exception.reason contains detailed information about
 *      the failure.
 */
+ (instancetype) modelWithDictionary:(NSDictionary *)dictionary;

/**
 * Basically calls modelWithDictionary: for an array of objects.
 *
 * @see modelWithDictionary:
 */
+ (NSArray *) arrayOfModelsWithArray:(NSArray *)array;

/**
 * Basically calls modelWithDictionary: for a set of objects.
 *
 * @see modelWithDictionary:
 */
+ (NSSet *) setOfModelsWithSet:(NSSet *)set;

/**
 * Basically calls modelWithDictionary: for a dictionary of objects.
 *
 * @see modelWithDictionary:
 */
+ (NSDictionary *) dictionaryOfModelsWithDictionary:(NSDictionary *)dict;

/**
 * Parses a model from json.
 */
+ (instancetype) modelWithJSONString:(NSString *)jsonString;

/**
 * Parses an array of models from json.
 */
+ (NSArray *) arrayOfModelsWithJSONArrayString:(NSString *)jsonArrayString;

/**
 * Parses a set of models from json.
 */
+ (NSSet *) setOfModelsWithJSONArrayString:(NSString *)jsonArrayString;

/**
 * Parses a dictionary of models from json.
 */
+ (NSDictionary *) dictionaryOfModelsWithJSONDictionaryString:(NSString *)jsonDictionaryString;

/**
 * Parses a model from NSData object.
 *
 * The data must have been created using serializeToData method.
 */
+ (instancetype) modelWithData:(NSData *)data;

/**
 * Reads model from file.
 *
 * The file must have been written using writeToFile method.
 */
+ (instancetype) modelWithFile:(NSString *)filePath;

/**
 * Serializes this object to a dictionary.
 */
- (NSDictionary *) serialize;

/**
 * Serializes this object to JSON.
 *
 * Calls [self serialize] and uses NSJSONSerialization to serialize the dictionary to JSON.
 */
- (NSString *) serializeToJSON;

/**
 * Serializes this model to NSData.
 */
- (NSData *) serializeToData;

/**
 * Writes this model to a file.
 */
- (void) writeToFile:(NSString *)filePath;

/**
 * Serializes the object and creates a new object by parsing the serialized data.
 *
 * This method is an alias for the copy method provided by the NSCopying protocol.
 */
- (instancetype) clone;

/**
 * This method is called before the object is parsed.
 */
- (void) beforeParse;

/**
 * This method is called after the object has been succesfully parsed.
 */
- (void) afterParse;

/**
 * This method is called before the serialization is started.
 */
- (void) beforeSerialize;

/**
 * This method is called after the object has been succesfully serialized.
 */
- (void) afterSerialize;

/**
 * Override this method if you do not want null values in serialization
 */
- (BOOL) allowNullValuesInSerialization;

@end
