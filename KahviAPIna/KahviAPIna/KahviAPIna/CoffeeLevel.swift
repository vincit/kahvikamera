//
//  CoffeeLevel.swift
//  KahviAPIna
//
//  Created by Saija Saarenpää on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

class CoffeeLevel: NSObject {
    var id: NSInteger = 0
    var level: Double = 0
    var time_stamp: String = ""
    
    init(id: NSInteger, level: Double, time_stamp: String) {
        self.id = id
        self.level = level
        self.time_stamp = time_stamp
    }
}
