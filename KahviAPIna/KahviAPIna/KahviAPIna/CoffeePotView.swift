//
//  CoffeePotView.swift
//  KahviAPIna
//
//  Created by Esa Väntsi on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

@IBDesignable
class CoffeePotView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet var coffeeLevelView: UIView!
    var view: UIView!
    var overlay: OverlayView?
    let nibName = "CoffeePotView"

    private var _percentage: Double = 100.0;
    private var _coffeeLevel: Double = 100.0;
    
    @IBOutlet var coffeeLevelHeight: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func awakeFromNib() {
        coffeeLevelView.layer.cornerRadius = 2.0
    }
    
    var coffeeLevel: Double! {
        get {
            return _coffeeLevel
        }
        set(value) {
            _coffeeLevel = value
            coffeeLevelHeight.constant = CGFloat((Double(62.0) * value)/Double(100.0))
        }
    }
    
    func showOverlay(message: String) {
        if (overlay != nil) {
            return
        }
        
        let nib = UINib(nibName: "OverlayView", bundle: nil)
        overlay = nib.instantiateWithOwner(self, options: nil)[0] as? OverlayView
        overlay!.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(overlay!)
        
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[overlay]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["overlay": overlay!]);
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[overlay]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["overlay": overlay!]);
        
        self.view.addConstraints(horizontalConstraints)
        self.view.addConstraints(verticalConstraints)
        overlay!.label = message

    }
    
    func hideOverlay() {
        self.overlay?.removeFromSuperview()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
}
