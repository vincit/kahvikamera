//
//  CoffeeManager.swift
//  KahviAPIna
//
//  Created by Saija Saarenpää on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit
import Alamofire

class CoffeeManager: NSObject {
    let serverUrl = "http://10.0.1.14:3000"
    static let sharedManager = CoffeeManager()
    
    func getCoffeeMakers(finished: (coffeeMakers: [CoffeeMaker]) -> Void) {
        let url = "\(serverUrl)/api/coffeeMaker/"
        
        
        Alamofire.request(.GET, url)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let objects = response.result.value as! [AnyObject!]
                    
                    var result: [CoffeeMaker] = []
                    for object in objects {
                        let coffee_pans = object["coffee_pan"] as! [[String : AnyObject]]
                        
                        var pans: [CoffeePot] = [];
                        for pan in coffee_pans {
                            if var level = pan["latest_level"] as? [String : AnyObject] {
                                let coffeeLevel = CoffeeLevel(id: level["id"] as! NSInteger, level: level["level"] as! Double, time_stamp: level["time_stamp"] as! String)
                                pans.append(CoffeePot(id: pan["id"] as! NSInteger, name: pan["name"] as! String, latest_level: coffeeLevel))
                                
                            }
                            else {
                                pans.append(CoffeePot(id: pan["id"] as! NSInteger, name: pan["name"] as! String, latest_level: nil))
                            }
                        }
                        
                        result.append(CoffeeMaker(id: object["id"] as! NSInteger, name: object["name"] as! String, location: object["location"] as! String, coffee_pan: pans))
                    }
                    
                    finished(coffeeMakers: result)
                }
                else {
                    print("Error getCoffeeMakers")
                }
        }
    }

    func getLevelForPot(potID: NSInteger, finished: (level: Double) -> Void) {
        let url = "\(serverUrl)/api/coffeePan/\(potID)/coffeeLevel/latest"
        
        Alamofire.request(.GET, url)
            .responseJSON { response in
                if response.result.isSuccess {
                    let objects: [AnyObject] = response.result.value as! [AnyObject!]
                    if (objects.count > 0) {
                        finished(level: objects[0]["level"] as! Double)

                    }
                }
                else {
                    print("Error getLevelForPot")
                }
        }
    }
}
