//
//  VINModel.m
//  ios-utilslib
//
//  Created by Sami Koskimäki on 10/3/13.
//  Copyright (c) 2013 Vincit Oy. All rights reserved.
//

#import <objc/runtime.h>

#import "VINModel.h"
#import "NSObject+VINReflection.h"


static NSString *const ErrorPathKey = @"fi.vincit.model.errorpath";
static NSString *const ParseError = @"ParseError";
static NSString *const SerializationError = @"SerializationError";


// Represents a mapping between a property and a class.
@interface VINObjectMapping : NSObject

@property (nonatomic, strong) NSString *propertyName;
@property (nonatomic, strong) NSString *keyInDictionary;
@property (nonatomic, strong) Class valueClass;
@property (nonatomic, strong) Class collectionClass;

+ (VINObjectMapping *) mappingForPrimitiveProperty:(NSString *)propertyName
                                   explicitMapping:(id)explicitMapping;

+ (VINObjectMapping *) mappingForProperty:(NSString *)propertyName
                            propertyClass:(Class)propertyClass
                          explicitMapping:(id)explicitMapping;

+ (VINObjectMapping *) mappingForCollectionProperty:(NSString *)propertyName
                                    collectionClass:(Class)collectionClass
                                    explicitMapping:(id)explicitMapping;

+ (BOOL) isExcludingExplicitMapping:(id)explicitMapping;

@end


@implementation VINModel


+ (NSDictionary *) explicitObjectMappings {
    return nil;
}


#pragma mark -
#pragma mark Parsing


+ (instancetype) modelWithDictionary:(NSDictionary *)dictionary {

    @try {
        return [VINModel parseScalar:dictionary ofClass:self];
    } @catch (NSException *exception) {
        [[VINModel buildExceptionWithErrorPath:exception inputObj:dictionary] raise];
    }

    // Never reached.
    return nil;
}

+ (NSArray *) arrayOfModelsWithArray:(NSArray *)array {

    @try {
        return [VINModel parseArray:array ofObjectsOfClass:self];
    } @catch (NSException *exception) {
        [[VINModel buildExceptionWithErrorPath:exception inputObj:array] raise];
    }

    // Never reached.
    return nil;
}

+ (NSSet *) setOfModelsWithSet:(NSSet *)set {

    @try {
        return [VINModel parseSet:set ofObjectsOfClass:self];
    } @catch (NSException *exception) {
        [[VINModel buildExceptionWithErrorPath:exception inputObj:set] raise];
    }

    // Never reached.
    return nil;
}

+ (NSDictionary *) dictionaryOfModelsWithDictionary:(NSDictionary *)dictionary {

    @try {
        return [VINModel parseDictionary:dictionary ofObjectsOfClass:self];
    } @catch (NSException *exception) {
        [[VINModel buildExceptionWithErrorPath:exception inputObj:dictionary] raise];
    }

    // Never reached.
    return nil;
}

+ (instancetype) modelWithJSONString:(NSString *)jsonString {

    return [self modelWithDictionary:[self parseJson:jsonString]];
}

+ (NSArray *) arrayOfModelsWithJSONArrayString:(NSString *)jsonArrayString {

    return [self arrayOfModelsWithArray:[self parseJson:jsonArrayString]];
}

+ (NSSet *) setOfModelsWithJSONArrayString:(NSString *)jsonArrayString {

    return [NSSet setWithArray:[self arrayOfModelsWithJSONArrayString:jsonArrayString]];
}

+ (NSDictionary *) dictionaryOfModelsWithJSONDictionaryString:(NSString *)jsonDictionaryString {

    return  [self dictionaryOfModelsWithDictionary:[self parseJson:jsonDictionaryString]];
}

+ (instancetype) modelWithData:(NSData *)data {

    return [self modelWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
}

+ (instancetype) modelWithFile:(NSString *)filePath {

    return [self modelWithData:[NSKeyedUnarchiver unarchiveObjectWithFile:filePath]];
}

+ (id) parseScalar:(id)obj ofClass:(Class)klass {

    if ([VINModel isNil:obj]) {
        return nil;
    }

    if ([klass isSubclassOfClass:[VINModel class]]) {

        return [VINModel parseModel:obj ofClass:klass];

    } else if ([VINModel isFoundationPrimitiveObjectClass:klass]) {

        return [VINModel parsePrimitive:obj ofClass:klass];

    }

    return nil;
}

+ (VINModel *) parseModel:(NSDictionary *)obj ofClass:(Class)klass {

    if ([VINModel isNil:obj]) {
        return nil;
    }

    if (![obj isKindOfClass:[NSDictionary class]]) {
        [NSException raise:ParseError format:@"expected dictionary got %@", [obj class]];
    }

    id model = [[klass alloc] init];

    [model parseObject:obj];

    return model;
}

- (void) parseObject:(NSDictionary *) obj {

    [self beforeParse];

    Class klass = [self class];

    NSDictionary *mappings = [klass cachedObjectMappings];

    [mappings enumerateKeysAndObjectsUsingBlock:^(NSString *propertyName, VINObjectMapping *mapping, BOOL *stop) {
        id value = obj[mapping.keyInDictionary];

        if (value == nil) {
            return;
        }

        @try {

            if ([mapping.collectionClass isSubclassOfClass:[NSArray class]]) {

                value = [VINModel parseArray:value ofObjectsOfClass:mapping.valueClass];

            } else if ([mapping.collectionClass isSubclassOfClass:[NSSet class]]) {

                value = [VINModel parseSet:value ofObjectsOfClass:mapping.valueClass];

            } else if ([mapping.collectionClass isSubclassOfClass:[NSDictionary class]]) {

                value = [VINModel parseDictionary:value ofObjectsOfClass:mapping.valueClass];

            } else {

                value = [VINModel parseScalar:value ofClass:mapping.valueClass];

            }

        } @catch (NSException *exception) {
            [[VINModel addPathComponent:mapping.keyInDictionary toException:exception] raise];
        }

        [self setValue:value forKey:mapping.propertyName];
    }];

    [self afterParse];
}

+ (NSArray *) parseArray:(NSArray *)arrayIn ofObjectsOfClass:(Class)klass {

    if ([VINModel isNil:arrayIn]) {
        return nil;
    }

    if (klass == nil) {
        return arrayIn;
    }

    if (![arrayIn isKindOfClass:[NSArray class]]) {
        [NSException raise:ParseError format:@"expected NSArray got %@", [arrayIn class]];
    }

    NSMutableArray* arrayOut = [NSMutableArray arrayWithCapacity:[arrayIn count]];

    for (NSInteger i = 0; i < [arrayIn count]; ++i) {
        @try {
            id obj = [VINModel parseScalar:arrayIn[i] ofClass:klass];
            [arrayOut addObject:obj ?: [NSNull null]];
        } @catch (NSException *exception) {
            [[VINModel addPathComponent:@(i) toException:exception] raise];
        }
    }

    return arrayOut;
}

+ (NSSet *) parseSet:(NSSet *)setIn ofObjectsOfClass:(Class)klass {

    if ([VINModel isNil:setIn]) {
        return nil;
    }

    if (klass == nil) {
        return setIn;
    }

    if (![setIn isKindOfClass:[NSSet class]]) {
        [NSException raise:ParseError format:@"expected NSSet got %@", [setIn class]];
    }

    NSMutableSet* setOut = [NSMutableSet setWithCapacity:[setIn count]];

    NSInteger i = 0;
    for (id object in setIn) {
        @try {
            id obj = [VINModel parseScalar:object ofClass:klass];
            [setOut addObject:obj ?: [NSNull null]];
        } @catch (NSException *exception) {
            [[VINModel addPathComponent:@(i) toException:exception] raise];
        }
        ++i;
    }

    return setOut;
}

+ (NSDictionary *) parseDictionary:(NSDictionary *)dictIn ofObjectsOfClass:(Class)klass {

    if ([VINModel isNil:dictIn]) {
        return nil;
    }

    if (klass == nil) {
        return dictIn;
    }

    if (![dictIn isKindOfClass:[NSDictionary class]]) {
        [NSException raise:ParseError format:@"expected NSDictionary got %@", [dictIn class]];
    }

    NSMutableDictionary* dictOut = [NSMutableDictionary dictionaryWithCapacity:[dictIn count]];

    for (id key in [dictIn allKeys]) {
        @try {
            id obj = [VINModel parseScalar:dictIn[key] ofClass:klass];
            dictOut[key] = obj ?: [NSNull null];
        } @catch (NSException *exception) {
            [[VINModel addPathComponent:key toException:exception] raise];
        }
    }

    return dictOut;
}

+ (id) parsePrimitive:(id)primitive ofClass:(Class)klass {

    if ([VINModel isNil:primitive]) {
        return nil;
    }

    if (![primitive isKindOfClass:klass]) {
        [NSException raise:ParseError format:@"expected %@ got %@", klass, [primitive class]];
    }

    return primitive;
}


#pragma mark -
#pragma mark Serialization


- (NSDictionary *) serialize {

    [self beforeSerialize];

    NSDictionary *mappings = [[self class] cachedObjectMappings];
    NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:mappings.count];

    [mappings enumerateKeysAndObjectsUsingBlock:^(NSString *propertyName, VINObjectMapping *mapping, BOOL *stop) {

        id value = [self valueForKey:propertyName];
        id serializedValue = nil;

        if ([VINModel isNil:value]) {
            if(![self allowNullValuesInSerialization]) {
                return;
            }
            serializedValue = [NSNull null];

        } else if ([mapping.collectionClass isSubclassOfClass:[NSArray class]]) {

            serializedValue = [VINModel serializeArray:value mapping:mapping];

        } else if ([mapping.collectionClass isSubclassOfClass:[NSSet class]]) {

            serializedValue = [VINModel serializeSet:value mapping:mapping];

        } else if ([mapping.collectionClass isSubclassOfClass:[NSDictionary class]]) {

            serializedValue = [VINModel serializeDictionary:value mapping:mapping];

        } else {

            serializedValue = [VINModel serializeScalar:value mapping:mapping];

        }

        obj[mapping.keyInDictionary] = serializedValue;
    }];

    [self afterSerialize];

    return obj;
}

- (NSString *) serializeToJSON {

    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[self serialize] options:kNilOptions error:&error];

    if (error) {
        [NSException raise:SerializationError format:@"%@", error.localizedDescription];
    }

    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];;
}

- (NSData *) serializeToData {

    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

- (void) writeToFile:(NSString *)filePath {

    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}

- (NSString *) description {

    return [[self serialize] description];
}

+ (NSArray *) serializeArray:(NSArray *)array mapping:(VINObjectMapping *)mapping {

    id serializedArray = nil;

    if ([mapping.valueClass isSubclassOfClass:[VINModel class]]) {
        serializedArray = [NSMutableArray arrayWithCapacity:[array count]];

        for (VINModel *model in array) {
            [serializedArray addObject:[model serialize]];
        }

    } else {
        serializedArray = [[NSMutableArray alloc] initWithArray:array copyItems:YES];
    }

    return serializedArray;
}

+ (NSSet *) serializeSet:(NSSet *)set mapping:(VINObjectMapping *)mapping {

    id serializedSet = nil;

    if ([mapping.valueClass isSubclassOfClass:[VINModel class]]) {
        serializedSet = [NSMutableSet setWithCapacity:[set count]];

        for (VINModel *model in set) {
            [serializedSet addObject:[model serialize]];
        }

    } else {
        serializedSet = [[NSMutableSet alloc] initWithSet:set copyItems:YES];
    }

    return serializedSet;
}

+ (NSDictionary *) serializeDictionary:(NSDictionary *)dict mapping:(VINObjectMapping *)mapping {

    id serializedDict = nil;

    if ([mapping.valueClass isSubclassOfClass:[VINModel class]]) {
        serializedDict = [NSMutableDictionary dictionaryWithCapacity:[dict count]];

        [dict enumerateKeysAndObjectsUsingBlock:^(NSString *key, VINModel *model, BOOL *stop) {
            serializedDict[key] = [model serialize];
        }];

    } else {
        serializedDict = [[NSMutableDictionary alloc] initWithDictionary:dict copyItems:YES];
    }

    return serializedDict;
}

+ (id) serializeScalar:(id)obj mapping:(VINObjectMapping *)mapping {

    if ([mapping.valueClass isSubclassOfClass:[VINModel class]]) {

        return [obj serialize];

    } else {

        return [obj copy];

    }
}


#pragma mark -
#pragma mark Other public methods


- (instancetype) clone {

    return [self copy];
}

- (void) beforeParse {
    // Do nothing by default.
}

- (void) afterParse {
    // Do nothing by default.
}

-(void) beforeSerialize {
    // Do nothing by default.
}

- (void) afterSerialize {
    // Do nothing by default.
}

- (BOOL) allowNullValuesInSerialization {
    // Allow null values by default
    return YES;
}



#pragma mark -
#pragma mark Exception handling


+ (NSException *) addPathComponent:(id)component toException:(NSException *)exception {
    NSMutableArray *path = exception.userInfo[ErrorPathKey];

    if (path == nil) {
        path = [NSMutableArray array];
    }

    [path addObject:component];

    return [NSException exceptionWithName:exception.name reason:exception.reason userInfo:@{ErrorPathKey : path}];
}

+ (NSException *)buildExceptionWithErrorPath:(NSException *)exception inputObj:(id)inputObj {
    NSArray *errorPathComponents = exception.userInfo[ErrorPathKey];

    NSMutableString *errorPath = [NSMutableString string];

    if (errorPathComponents.count) {
        for (NSInteger i = errorPathComponents.count - 1; i >= 0; --i) {
            id component = errorPathComponents[i];

            if ([component isKindOfClass:[NSString class]]) {
                if (errorPath.length) {
                    [errorPath appendFormat:@".%@", component];
                } else {
                    [errorPath appendString:component];
                }
            } else {
                [errorPath appendFormat:@"[%@]", component];
            }
        }
    }

    NSString *reason = nil;
    if (errorPath.length == 0) {
        reason = [NSString stringWithFormat:@"%@\nObject: %@", exception.reason, inputObj];
    } else {
        reason = [NSString stringWithFormat:@"%@: %@\nObject: %@", errorPath, exception.reason, inputObj];
    }

    return [NSException exceptionWithName:exception.name reason:reason userInfo:exception.userInfo];
}


#pragma mark -
#pragma mark Object mapping stuff


+ (NSDictionary *) cachedObjectMappings {

    static NSMutableDictionary *mappingCache = nil;
    NSString *const className = NSStringFromClass(self);

    @synchronized ([VINModel class]) {

        if (mappingCache == nil) {
            mappingCache = [NSMutableDictionary dictionary];
        }

        if (mappingCache[className] == nil) {
            mappingCache[className] = [self createObjectMappings];
        }

        return mappingCache[className];
    }
}

+ (NSDictionary *) createObjectMappings {

    NSDictionary *explicitMappings = [self explicitObjectMappings];
    NSMutableDictionary *objectMappings = [NSMutableDictionary dictionary];
    NSSet *propertyNames = [NSSet setWithArray:[self propertyNamesUpToSuperclass:[VINModel class]]];

    [explicitMappings enumerateKeysAndObjectsUsingBlock:^(NSString *propertyName, id obj, BOOL *stop) {

        if (![propertyNames containsObject:propertyName]) {

            [NSException raise:@"InvalidMappingError"
                        format:@"The class %@ doesn't have property named \"%@\", but there is an explicit mapping for it",
             [self class], propertyName];
        }
    }];

    for (NSString *propertyName in propertyNames) {

        VINObjectMapping *mapping = nil;

        mapping = [self createObjectMappingForProperty:propertyName explicitMapping:explicitMappings[propertyName]];

        if (mapping != nil) {

            objectMappings[propertyName] = mapping;
        }
    }

    return objectMappings;
}

+ (VINObjectMapping *) createObjectMappingForProperty:(NSString *)propertyName explicitMapping:(id)explicitMapping {

    if ([VINObjectMapping isExcludingExplicitMapping:explicitMapping]) {

        // This property is excluded (its explicit mapping says that is should be excluded).
        return nil;
    }

    Class propertyClass = [self classOfProperty:propertyName];

    if (propertyClass == nil) {

        // nil property class means the property is a primitive.
        return [VINObjectMapping mappingForPrimitiveProperty:propertyName explicitMapping:explicitMapping];

    } else if ([self isFoundationCollectionClass:propertyClass]) {

        // Collection property.
        return [VINObjectMapping mappingForCollectionProperty:propertyName collectionClass:propertyClass explicitMapping:explicitMapping];

    } else if ([self isFoundationPrimitiveObjectClass:propertyClass]) {

        // Foundation primitive objects like NSString, NSNumber etc.
        return [VINObjectMapping mappingForProperty:propertyName propertyClass:propertyClass explicitMapping:explicitMapping];

    } else if ([propertyClass isSubclassOfClass:[VINModel class]]) {

        // Subclasses of VINModel.
        return [VINObjectMapping mappingForProperty:propertyName propertyClass:propertyClass explicitMapping:explicitMapping];

    } else {

        [NSException raise:@"InvalidMappingError" format:@"mapping %@ for property %@ is invalid.", explicitMapping, propertyName];

    }

    return nil;
}

+ (BOOL) isFoundationCollectionClass:(Class)klass {

    return [klass isSubclassOfClass:[NSArray class]]
    || [klass isSubclassOfClass:[NSSet class]]
    || [klass isSubclassOfClass:[NSDictionary class]];
}

+ (BOOL) isFoundationPrimitiveObjectClass:(Class)klass {

    return [klass isSubclassOfClass:[NSNumber class]]
    || [klass isSubclassOfClass:[NSString class]]
    || [klass isSubclassOfClass:[NSDate class]];
}

+ (BOOL) isSerializableClass:(Class)klass {

    return [self isFoundationCollectionClass:klass]
    || [self isFoundationPrimitiveObjectClass:klass]
    || [klass isSubclassOfClass:[VINModel class]];
}


#pragma mark -
#pragma mark Helper stuff


+ (BOOL) isNil:(id)obj {
    return obj == nil || [[NSNull null] isEqual:obj];
}

+ (id) parseJson:(NSString *)json {
    NSError *error;
    id obj = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                             options:kNilOptions error:&error];

    if (error) {
        [NSException raise:ParseError format:@"%@", error.localizedDescription];
    }

    return obj;
}


#pragma mark -
#pragma mark NSCoding methods


- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    if (self) {
        [self parseObject:[aDecoder decodeObject]];
    }

    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self serialize]];
}


#pragma mark -
#pragma mark NSCopying methods


- (id) copyWithZone:(NSZone *)zone {

    id obj = [[[self class] allocWithZone:zone] init];

    [obj parseObject:[self serialize]];

    return obj;
}


@end



#pragma mark -
#pragma mark VINObjectMapping implementation



@implementation VINObjectMapping


+ (VINObjectMapping *) mappingForPrimitiveProperty:(NSString *)propertyName
                                   explicitMapping:(id)explicitMapping {

    // All primitive values can be expressed as NSNumber.
    return [self mappingForProperty:propertyName propertyClass:[NSNumber class] explicitMapping:explicitMapping];

}

+ (VINObjectMapping *) mappingForProperty:(NSString *)propertyName
                            propertyClass:(Class)propertyClass
                          explicitMapping:(id)explicitMapping {

    VINObjectMapping *mapping = [VINObjectMapping mappingForExplicitMapping:explicitMapping];

    if (!mapping) {
        return nil;
    }

    mapping.propertyName = propertyName;
    mapping.valueClass = propertyClass;

    return mapping;
}

+ (VINObjectMapping *) mappingForCollectionProperty:(NSString *)propertyName
                                    collectionClass:(Class)collectionClass
                                    explicitMapping:(id)explicitMapping {

    VINObjectMapping *mapping = [VINObjectMapping mappingForExplicitMapping:explicitMapping];

    if (!mapping) {
        return nil;
    }

    mapping.propertyName = propertyName;
    mapping.collectionClass = collectionClass;

    return mapping;
}

// Returns YES if the explicit mapping says that the property should be excluded.
+ (BOOL) isExcludingExplicitMapping:(id)explicitMapping {

    return [explicitMapping isEqual:@NO];
}

+ (VINObjectMapping *) mappingForExplicitMapping:(id)explicitMapping {

    VINObjectMapping *mapping = [[VINObjectMapping alloc] init];

    if (explicitMapping == nil) {

        return mapping;
    }

    if (![explicitMapping isKindOfClass:[NSArray class]]) {

        explicitMapping = @[explicitMapping];
    }

    for (id explicitMappingItem in explicitMapping) {

        if ([VINObjectMapping isExcludingExplicitMapping:explicitMappingItem]) {

            // The property is excluded.
            return nil;

        } else if ([explicitMappingItem isKindOfClass:[NSString class]]) {

            mapping.keyInDictionary = explicitMappingItem;

        } else if (class_isMetaClass(object_getClass(explicitMappingItem))) {

            mapping.valueClass = explicitMappingItem;

        } else {

            [NSException raise:@"InvalidMappingError" format:@"mapping %@ is invalid.", explicitMapping];
        }
    }

    if (mapping.valueClass && ![VINModel isSerializableClass:mapping.valueClass]) {
        
        [NSException raise:@"InvalidMappingError" format:@"mapping %@ is invalid.", explicitMapping];
    }
    
    return mapping;
}

- (NSString *) keyInDictionary {
    
    // Default to propertyName.
    if (_keyInDictionary == nil) {
        
        return _propertyName;
    }
    
    return _keyInDictionary;
}

- (NSString *) description {
    
    return [NSString stringWithFormat:@"%@ %@ %@ %@",
            self.propertyName,
            self.keyInDictionary,
            self.valueClass,
            self.collectionClass];
}

@end
