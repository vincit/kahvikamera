//
//  OverlayView.swift
//  KahviAPIna
//
//  Created by Esa Väntsi on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit


@IBDesignable class OverlayView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet var text: UILabel!
    var view: UIView!
    
    var label: String {
        get {
            return text.text ?? ""
        }
        set(value) {
            text.text = value
        }
    }
}
