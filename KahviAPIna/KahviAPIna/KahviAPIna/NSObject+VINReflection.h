//
//  NSObject+VINReflection.h
//  ios-utilslib
//
//  Created by Sami Koskimäki on 10/3/13.
//  Copyright (c) 2013 Vincit Oy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (VINReflection)

/**
 * Returns the names of all the properties of this class.
 *
 * @note This method doesn't return the properties of the superclasses. Use propertyNames
 *       method to get also the superclasses' property names.
 */
+ (NSArray *) ownPropertyNames;

/**
 * Returns the names of all the properties of this class and its superclasses.
 */
+ (NSArray *) propertyNames;

/**
 * Returns the names of all the properties of this class and its superclasses up to 'superclass'.
 *
 * The properties of 'superclass' are not included.
 */
+ (NSArray *) propertyNamesUpToSuperclass:(Class)superclass;

/**
 * Returns the class of a property.
 *
 * Returns nil if property is a primitive.
 */
+ (Class) classOfProperty:(NSString *)propertyName;

@end
