//
//  WingView.swift
//  KahviAPIna
//
//  Created by Esa Väntsi on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

@IBDesignable
class WingView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet var leftCoffeePot: CoffeePotView!
    @IBOutlet var rightCoffeePot: CoffeePotView!
    @IBOutlet var headerLabel: UILabel!
    
    var view: UIView!
    private var _model: CoffeeMaker?
    
    @IBInspectable var header: String = "Unnamed Wing D:" {
        didSet {
            headerLabel.text = header
        }
    }
    
    var model: CoffeeMaker? {
        get {
            return _model
        }
        set(value) {
            _model = value
        }
    }
    
    init(frame: CGRect, model: CoffeeMaker) {
        super.init(frame: frame)
        headerLabel.text = "\(model.name) \(model.location)"
        self.model = model
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "WingView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    func update() {
        if (model == nil) {
            return
        }
        let manager: CoffeeManager = CoffeeManager.sharedManager;
        
        manager.getLevelForPot((model?.coffee_pan![0] as! CoffeePot).id, finished: {
            [unowned self]
            (level) -> Void in
            self.leftCoffeePot.coffeeLevel = level
        })
        
        manager.getLevelForPot((model?.coffee_pan![1] as! CoffeePot).id, finished: {
            [unowned self]
            (level) -> Void in
            self.rightCoffeePot.coffeeLevel = level
        })
    }
}
