//
//  CoffeeMaker.swift
//  KahviAPIna
//
//  Created by Saija Saarenpää on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

class CoffeeMaker: NSObject {
    var id: NSInteger = 0
    var name: String = ""
    var location: String = ""
    var coffee_pan: NSArray?
    
    init(id: NSInteger, name: String, location: String, coffee_pan: [AnyObject]) {
        self.id = id
        self.name = name
        self.location = location
        self.coffee_pan = coffee_pan
    }
}
