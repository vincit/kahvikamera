//
//  ViewController.swift
//  KahviAPIna
//
//  Created by Saija Saarenpää on 28/11/15.
//  Copyright © 2015 kahvikamera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var AWingView: WingView!
    @IBOutlet var BWingView: WingView!
    @IBOutlet var CWingView: WingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let manager: CoffeeManager = CoffeeManager.sharedManager;
        
        manager.getCoffeeMakers {
            [unowned self]
            (coffeeMakers) -> Void in
            self.AWingView.model = coffeeMakers[0]
            self.BWingView.model = coffeeMakers[1]
            self.CWingView.model = coffeeMakers[2]
            self.periodicUpdate()
        }
        self.periodicUpdate()

        manager.getLevelForPot(0,finished: {(level: Double) -> Void in print("Level: " + String(level)) });
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func periodicUpdate() {
        AWingView.update()
        BWingView.update()
        CWingView.update()
        
        let delay = 1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) {
            [unowned self] in
            self.periodicUpdate()
        }
    }
}

