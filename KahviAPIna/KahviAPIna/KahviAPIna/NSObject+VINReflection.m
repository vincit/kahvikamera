//
//  NSObject+VINReflection.m
//  ios-utilslib
//
//  Created by Sami Koskimäki on 10/3/13.
//  Copyright (c) 2013 Vincit Oy. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+VINReflection.h"

@implementation NSObject (VINReflection)

+ (NSArray *) ownPropertyNames {
    unsigned int count = 0;

    objc_property_t *properties = class_copyPropertyList(self, &count);

    if (count == 0) {
        free(properties);
        return nil;
    }

    NSMutableArray *propertyNames = [NSMutableArray arrayWithCapacity:count];
    
    for (unsigned int i = 0; i < count; i++) {
        [propertyNames addObject:[NSString stringWithUTF8String:property_getName(properties[i])]];
    }

    return propertyNames;
}

+ (NSArray *) propertyNamesUpToSuperclass:(Class)superclass {

    NSMutableArray *propertyNames = [NSMutableArray array];
    Class klass = self;

    // Travel up the inheritance tree and collect properties of all ancestor classes up to 'superclass'.
    while (klass != nil && klass != superclass) {

        NSArray *properties = [klass ownPropertyNames];
        [propertyNames addObjectsFromArray:properties];
        klass = [klass superclass];
    }

    // Some versions of iOS/objective-c also give the NSObject properties from ownPropertyNames method.
    // Make sure they are removed.
    [propertyNames removeObjectsInArray:@[@"description", @"debugDescription", @"superclass", @"hash"]];

    return propertyNames;
}

+ (NSArray *) propertyNames {

    return [self propertyNamesUpToSuperclass:nil];
}

+ (Class) classOfProperty:(NSString *)propertyName {
    objc_property_t property = class_getProperty(self, [propertyName UTF8String]);

    const char *attrs = property_getAttributes(property);
    if (attrs == NULL) {
        return nil;
    }

    char *type = property_copyAttributeValue(property, "T");
    if (type == NULL) {
        free(type);
        return nil;
    }

    const char *start = strchr(type, '"');
    char *end = strrchr(type, '"');

    if (start == NULL || end == NULL || start == end) {
        free(type);
        return nil;
    }

    *end = '\0';
    NSString *className = [NSString stringWithUTF8String:start + 1];
    free(type);

    return NSClassFromString(className);
}

@end
