# README #


### What is this repository for? ###

Kahvikamera is for monitoring and reporting coffee level of coffee makers. Contains source code for OpenCV/Python coffee level detector and Clojure/Angular reporting server.

### How do I get set up? ###

* For OpenCV development you need Python with OpenCV and Numpy.
* For reporting server development/build you need Leiningen, Postgres, Gulp and Bower.

Docker build:
1) Build using server/build.sh
2) Go to server/kahvikamera_server/
3) Run docker-compose build


### Who do I talk to? ###

* turkka.mannila@vincit.fi / turkka.mannila @ slack