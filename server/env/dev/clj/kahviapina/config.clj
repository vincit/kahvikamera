(ns kahviapina.config
  (:require [selmer.parser :as parser]
            [taoensso.timbre :as timbre]
            [kahviapina.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (timbre/info "\n-=[kahviapina started successfully using the development profile]=-"))
   :middleware wrap-dev})
