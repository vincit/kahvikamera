(ns kahviapina.config
  (:require [taoensso.timbre :as timbre]))

(def defaults
  {:init
   (fn []
     (timbre/info "\n-=[kahviapina started successfully]=-"))
   :middleware identity})
