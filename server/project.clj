(defproject kahviapina "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "http://example.com/FIXME"

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.apache.commons/commons-math3 "3.6"]
                 [selmer "0.9.5"]
                 [markdown-clj "0.9.82"]
                 [environ "1.0.1"]
                 [metosin/ring-middleware-format "0.6.0"]
                 [metosin/ring-http-response "0.6.5"]
                 [bouncer "0.3.3"]
                 [net.mikera/core.matrix "0.51.0"]
                 [net.mikera/core.matrix.stats "0.7.0"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [org.clojure/tools.nrepl "0.2.12"]
                 [org.webjars/bootstrap "3.3.5"]
                 [org.webjars/jquery "2.1.4"]
                 [com.taoensso/tower "3.0.2"]
                 [com.taoensso/timbre "4.1.4"]
                 [com.fzakaria/slf4j-timbre "0.2.1"]
                 [compojure "1.4.0"]
                 [ring-webjars "0.1.1"]
                 [ring/ring-defaults "0.1.5"]
                 [ring-ttl-session "0.3.0"]
                 [ring "1.4.0" :exclusions [ring/ring-jetty-adapter]]
                 [mount "0.1.4" :exclusions [ch.qos.logback/logback-classic]]
                 [migratus "0.8.7"]
                 [conman "0.2.7"]
                 [org.postgresql/postgresql "9.4-1203-jdbc41"]
                 [org.clojure/java.jdbc "0.3.7"]
                 [korma "0.4.2"]
                 [org.clojure/clojurescript "1.7.170" :scope "provided"]
                 [reagent "0.5.1"]
                 [reagent-forms "0.5.13"]
                 [reagent-utils "0.1.5"]
                 [secretary "1.2.3"]
                 [org.clojure/core.async "0.2.374"]
                 [metosin/compojure-api "0.24.0"]
                 [metosin/ring-swagger-ui "2.1.3-2"]
                 [http-kit "2.1.19"]
                 [clj-http "2.0.0"]
                 [prismatic/schema "1.0.3"]]

  :min-lein-version "2.0.0"
  :uberjar-name "kahviapina.jar"
  :jvm-opts ["-server"]
  :resource-paths ["resources" ]

  :main kahviapina.core
  :migratus {:store :database}

  :plugins [[lein-environ "1.0.1"]
            [migratus-lein "0.2.0"]
            [cider/cider-nrepl "0.9.1"]
            [refactor-nrepl "1.1.0"]]
  :clean-targets ^{:protect false} [:target-path [:jsbuild :builds :app :compiler :output-dir] [:jsbuild :builds :app :compiler :output-to]]

  :profiles
  {:uberjar       {:omit-source  true
                   :env          {:production true :db-host "possu"}
                   :prep-tasks   ["compile"]

                   :aot          :all
                   :source-paths ["env/prod/clj"]}
   :dev           [:project/dev :profiles/dev]
   :prod          [:profiles/prod]
   :test          [:project/test :profiles/test]
   :project/dev   {:dependencies [[prone "0.8.2"]
                                  [ring/ring-mock "0.3.0"]
                                  [ring/ring-devel "1.4.0"]
                                  [pjstadig/humane-test-output "0.7.0"]
                                  [com.cemerick/piggieback "0.2.2-SNAPSHOT"]
                                  [lein-figwheel "0.5.0-2"]
                                  [mvxcvi/puget "1.0.0"]]
                   :plugins      [[lein-figwheel "0.5.0-2"] [cider/cider-nrepl "0.9.1"]]

                   :source-paths ["env/dev/clj"]
                   :repl-options {:init-ns kahviapina.core}
                   :injections   [(require 'pjstadig.humane-test-output)
                                  (pjstadig.humane-test-output/activate!)]
                   ;;when :nrepl-port is set the application starts the nREPL server on load
                   :env          {:dev        true
                                  :port       3000
                                  :nrepl-port 7000
                                  :log-level  :trace}}
   :project/test  {:env {:test       true
                         :port       3001
                         :nrepl-port 7001
                         :log-level  :trace}}
   :profiles/dev  {}
   :profiles/test {}})
