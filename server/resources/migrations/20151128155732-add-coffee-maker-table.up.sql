CREATE TABLE coffee_maker (
    id BIGSERIAL PRIMARY KEY,
    location VARCHAR(256),
    name VARCHAR(256)
);