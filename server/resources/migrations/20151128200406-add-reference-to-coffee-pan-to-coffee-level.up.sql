ALTER TABLE coffee_pan ADD PRIMARY KEY (id);
ALTER TABLE coffee_level ADD COLUMN coffee_pan_id bigint REFERENCES coffee_pan(id);