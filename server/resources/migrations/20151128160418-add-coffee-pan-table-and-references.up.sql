CREATE TABLE coffee_pan (
    id BIGSERIAL,
    name VARCHAR(256),
    coffee_maker_id BIGINT REFERENCES coffee_maker (id)
);