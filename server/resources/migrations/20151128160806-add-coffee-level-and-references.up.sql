CREATE TABLE coffee_level (
    id BIGSERIAL,
    time_stamp timestamp with time zone,
    level real
);