(function() {
  'use strict';

  angular
    .module('app', ['ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'toastr',
    'gridshore.c3js.chart',
    'ngWebSocket',
    'main',
    'calibrate',
    'calibrate.services',
    'coffee.resource',
    'visibilityChange']);

})();
