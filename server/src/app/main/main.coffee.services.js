(function() {
  'use strict';
  angular.module('coffee.resource', [])
    .factory('CoffeeMakerResource', CoffeeMakerResource)
    .factory('CoffeeLevelResource', CoffeeLevelResource)
    .factory('CoffeePanResource', CoffeePanResource);

  /** @ngInject */
  function CoffeeMakerResource($resource) {
      return $resource('/api/coffeeMaker');
  }

  /** @ngInject */
  function CoffeePanResource($resource) {
    return $resource('/api/coffeePan/:id', {'id' : '@id'}, {
      update: {
        method: 'PUT'
      }
    });
  }

  /** @ngInject */
  function CoffeeLevelResource($resource) {
    return $resource('/api/coffeePan/:id/coffeeLevel/:sub', {'id' : '@id'}, {
      latestForPan: {
        method: 'GET',
        isArray: true,
        params: {
          sub: 'latest'
        }
      },
      coffeeLevelHistory: {
        method: 'GET',
        isArray: true,
        params: {
          sub: 'history'
        }
      }
    });
  }
})();
