

(function() {
  'use strict';

  angular
    .module('main', ['coffee.resource'])
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, toastr, CoffeeMakerResource, CoffeeLevelResource, $websocket, VisibilityChange, $log) {
    var vm = this;
    var coffeeMakers;
    vm.coffeePanLevels = {};

    vm.getPanColsNumber = function(maker) {
      return maker.coffee_pan.length > 0 ? Math.floor(12/maker.coffee_pan.length) : 12;
    };

    vm.getPanLevel = function(panId) {
      return vm.coffeePanLevels[panId] ? Math.round(vm.coffeePanLevels[panId],2) + '%' : '0%';
    };


    function loadCoffeeMakers() {
      CoffeeMakerResource.query().$promise.then(setCoffeeMakers);
    }

    loadCoffeeMakers();

    var makerIdByPanId = {};
    var chartsByPanId = {};
    var gaugesByPanId = {};
    var coffeePansById = {};

    function generatePanGauges(pans) {

      _.forEach(pans, function(pan) {
        var elemId = 'pan-' + pan.id;
        gaugesByPanId[pan.id] = c3.generate({
          bindto: '#' + elemId,
          data: {
            type: 'gauge',
            columns: [['coffee level', 0]]
          },
          size: {
            height: 180
          },
          color: {
                  pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                  threshold: {
                      values: [30, 60, 70, 100]
                  }
          },
          gauge: {
            label: {
              format: function(d) {return Math.round(d) + '%';}
            }
          },
        });
      });

    }

    function generateMakerCharts(makers) {
      _.forEach(makers, function(maker) {
        $log.log(maker);

        var allColors =
            ['#4D4D4D',
            '#5DA5DA',
            '#FAA43A',
            '#60BD68',
            '#F17CB0',
            '#B2912F',
            '#B276B2',
            '#DECF3F',
            '#F15854'];

        _.forEach(maker.coffee_pan, function(pan) {

          var elemId = 'panchart-' + pan.id;
          var colors = {};
          colors[pan.name] = allColors[pan.id % allColors.length];
          var chart = c3.generate({
            bindto: '#' + elemId,
            axis: {
              x: {
                type: 'timeseries',
                tick: {
                  format: '%H:%M'
                },
                padding: {
                  right: 0
                }
              },
              y: {
                min: 0.0,
                max: 100.0,
                "default": [0,100],
                padding: {
                  top: 0,
                  bottom: 0
                },
                tick: {
                  format: function(d) {return Math.round(d) + '%';}
                }
              }
            },
            data: {
              x: 'x',
              type: 'area',
              columns: [['x'], [pan.name]],
              colors: colors
            },
            point: {
              show: false
            }

          });
          chartsByPanId[pan.id] = chart;
        });

        generatePanGauges(maker.coffee_pan);

      });
    };

    function loadPanDataChartData(pan, data) {
      var chart = chartsByPanId[pan.id];
      var name = pan.name;
      var xs = ['x'].concat(_(data).map(function(d) { return Date.parse(d.time_stamp);}).value());
      var ys = [name].concat(_(data).map(function(d) {return d.level;}).value());
      chart.load({
        columns:[xs, ys]
      });

      if (data.length > 0) {
        setGaugeLevel(pan.id, data[data.length - 1]);
      }

    }


    function pushPanData(pan, levelObj) {
      var chart = chartsByPanId[pan.id];
      chart.flow({
         columns: [['x', Date.parse(levelObj.time_stamp)], [pan.name, levelObj.level]]
      });
    }

    var panLevelStreams = {};

    function disconnectSockets() {
      _(_.values(panLevelStreams)).forEach(function(socket) {
        socket.close();
      }).value();
    }

    function subscribeToStream(pan) {
      $log.log("Connecting socket for ", pan.id);

      var scheme = 'ws';

      if (window.location.protocol.indexOf('https') > -1) {
        scheme = 'wss';
      }

      var ws = $websocket( scheme + '://' + window.location.host  + '/channels/coffee-pan-level/' + pan.id);
      ws.onMessage(function(msg) {
        $log.log(msg);
        var levelObj = JSON.parse(msg.data);
        setGaugeLevel(pan.id, levelObj);
        pushPanData(pan, levelObj);
      });

      panLevelStreams[pan.id] = ws;
    }

    function loadHistory(pan) {
      CoffeeLevelResource.coffeeLevelHistory({id: pan.id}).$promise.then(function(resp) {
        loadPanDataChartData(pan, resp);
        subscribeToStream(pan);
      });
    }

    function setGaugeLevel(panId, level) {
      var gauge = gaugesByPanId[panId];
      if (gauge) {
        gauge.load({columns: [['coffee level', level.level]]});
      }
    };

    function loadDataAndConnectSockets() {
      _.forEach(coffeeMakers, function(maker) {

        _.forEach(maker.coffee_pan, loadHistory);
      });
    }

    var initialized = false;

    function setCoffeeMakers(makers) {
      coffeeMakers = makers;
      vm.coffeeMakers = makers;
      _.forEach(makers, function(maker) {
        _.forEach(maker.coffee_pan, function(pan) {
          makerIdByPanId[pan.id] = maker.id;
        });
        _.forEach(maker.coffee_pan, function (pan) {coffeePansById[pan.id] = pan;});
      });

      //Timeout runs after digest cycle so it's safe to assume that view is rendered
      $timeout(function() {
        generateMakerCharts(makers);
        initialized = true;
        $log.log("Loading initial data");
        loadDataAndConnectSockets();
      }, 1);
    }

    VisibilityChange.onVisible(function() {
      if(initialized) {
        $log.log("Loading data onVisible");
        loadDataAndConnectSockets();
      }
    });

    VisibilityChange.onHidden(function() {
      if(initialized) {
        $log.log("Disconnecting sockets");
        disconnectSockets();
      }
    });

  }
})();

