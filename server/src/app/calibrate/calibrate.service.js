(function() {
  'use strict';
  angular.module('calibrate.services', ['coffee.resource'])
    .factory('CalibrationCanvas', CalibrationCanvas)

  /** @ngInject */
  function CalibrationCanvas() {
    return function(canvas) {
      var c = {};
      console.log(canvas);
      c.canvas = canvas;
      c.context = canvas.getContext('2d');

      var ctx = c.context;

      function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: Math.floor(evt.clientX - rect.left),
          y: Math.floor(evt.clientY - rect.top)
        };
      }

      function drawPan(pan) {
          var width = pan.cap_x2 - pan.cap_x1;
          var height = pan.cap_y2 - pan.cap_y1;
          ctx.beginPath();
          ctx.strokeStyle = c.selectedPan && c.selectedPan.id == pan.id ? "#00FF00" : "#FF0000";
          ctx.lineWidth = 6;
          ctx.rect(pan.cap_x1, pan.cap_y1, width, height);
          ctx.stroke();
          ctx.closePath();
      };

      function draw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        _.forEach(_.values(c.pansById), drawPan);
      };

      c.setPans = function(pans) {
        console.log(pans);
        c.pansById = _.zipObject(_.map(pans, function (pan) { return pan.id;}), pans);
        draw();
      };

      c.selectPan = function(pan) {
        c.selectedPan = pan;
        c.pansById[pan.id] = pan;
        draw();
      };

      var updateX;
      var updateY;

      canvas.onmousemove = function(event) {
        var coords = getMousePos(canvas, event);
        if (updateX && updateY) {
          updateX(coords.x);
          updateY(coords.y);
          draw();
        }
      };

      canvas.onmousedown = function(event) {
        var coords = getMousePos(canvas, event);
        if (c.selectedPan) {
          c.selectedPan.cap_x1 = coords.x;
          c.selectedPan.cap_y1 = coords.y;
          updateX = function(v) {c.selectedPan.cap_x2 = v;};
          updateY = function(v) {c.selectedPan.cap_y2 = v;};
        }
      };

      canvas.onmouseup = function() {
        updateX = null;
        updateY = null;
      };


      return c;
    }
  }
})();
