(function() {
  'use strict';

  angular
    .module('calibrate', ['coffee.resource', 'calibrate.services'])
    .controller('CalibrateController', CalibrateController);

  /** @ngInject */
  function CalibrateController(CoffeeMakerResource, $timeout, CalibrationCanvas, CoffeePanResource, $q) {
    var vm = this;

    vm.selectedMaker = undefined;

    function loadMakers() {
      CoffeeMakerResource.query().$promise.then(function(makers) {
        vm.makers = makers;

      });
    }

    loadMakers();

    vm.selectMaker = function(maker) {
      vm.selectedMaker = maker;
      canvas.setPans(maker.coffee_pan);
    };

    vm.selectPan = function(pan) {
      vm.selectedPan = pan;
      canvas.selectPan(pan);
    };

    vm.savePans = function(maker) {
      var promises = _.map(maker.coffee_pan, function(pan) {
        return CoffeePanResource.update(pan).$promise;
      });
      $q.all(function() {
        alert("Settings saved");
      });
    };

    var canvas;
    var reloadImageTimeout;

    $timeout(function() {
      canvas = CalibrationCanvas(angular.element('#calibCanvas')[0]);
      var image = angular.element('#calibImage')[0];
      console.log(image);



      function reloadImage() {
        reloadImageTimeout = $timeout(reloadImage, 1000);
        if (vm.selectedMaker) {
          image.src = '/api/coffeeMaker/' + vm.selectedMaker.id + '/image?time=' + Date.now();
        }

      }
      reloadImageTimeout = $timeout(reloadImage, 1000);

    }, 0);

  }

})();
