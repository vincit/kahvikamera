(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('calibrate', {
        url: '/calibrate',
        templateUrl: 'app/calibrate/calibrate.html',
        controller: 'CalibrateController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
