(ns kahviapina.db.core
  (:require [environ.core :refer [env]]
            [taoensso.timbre :as log])
  (:use [korma.db]
        [korma.core])
  (:import (java.time Instant)
           (java.time.temporal ChronoUnit)
           (java.sql Timestamp)))

(defn to-sql-timestamp [date]
  (new java.sql.Timestamp (.getTime date)))

(defn to-util-date [date]
  (new java.util.Date (.getTime date)))


(defdb db (let [db-host (or (System/getenv "DB_HOST") "localhost")]
            (log/info "Using database " db-host)
            (postgres {:host     db-host
                       :db       (or (System/getenv "DB") "kahvikamera")
                       :user     (or (System/getenv "DB_USER") "kahvikamera")
                       :password (or (System/getenv "DB_PASSWORD") "kahvikamera")})))



(defentity coffee-level
           (table "coffee_level")
           (entity-fields :id :level :time_stamp :coffee_pan_id))

(defentity coffee-pan
           (entity-fields :id :name :coffee_maker_id :cap_x1 :cap_y1 :cap_x2 :cap_y2)
           (table "coffee_pan")
           (has-many coffee-level))

(defentity coffee-maker
           (entity-fields :id :name :location)
           (table "coffee_maker")
           (has-many coffee-pan))


;CoffeMakers

(defn create-coffee-maker [maker]
  (insert coffee-maker (values maker)))

(defn update-coffee-maker [id maker]
  (korma.core/update coffee-maker
                     (set-fields maker)
                     (where {:id [= id]})))

(defn list-coffee-makers []
  (select coffee-maker
          (with coffee-pan)))

(defn get-coffee-maker [id]
  (first (select coffee-maker
                 (where {:id id})
                 (with coffee-pan))))

;CoffeePans
(defn create-coffee-pan [pan]
  (insert coffee-pan (values pan)))

(defn update-coffee-pan [id pan]
  (korma.core/update coffee-pan
                     (set-fields pan)
                     (where {:id [= id]}))
  (first (select coffee-pan (where {:id [= id]}))))

;CoffeeLevel

(defn hours-ago [hours]
  (Timestamp/from (.minus (Instant/now) hours ChronoUnit/HOURS)))

(defn minutes-ago [minutes]
  (Timestamp/from (.minus (Instant/now) minutes ChronoUnit/MINUTES)))


(defn get-coffee-history [pan-id]
  (select coffee-level
          (where {:coffee_pan_id pan-id :time_stamp [> (minutes-ago 30)]})
          (order :time_stamp :ASC)))

(defn list-coffee-level []
  (select coffee-level))

(defn create-coffee-level [level]
    (insert coffee-level (values (update-in level [:time_stamp] #(Timestamp/from %)))))

(defn get-latest-coffee-level-for-pan [pan-id]
  (->> (select coffee-level
              (where {:coffee_pan_id pan-id})
              (order :time_stamp :DESC)
              (limit 1))
      (map (fn [level] (update-in level
                                  [:time_stamp]
                                  str)))))

(comment
  (list-coffee-makers)
  (create-coffee-maker {:name "Mogga" :location "hackfest"})
  (create-coffee-pan {:name "test" :coffee_maker_id 6})
  (update-coffee-maker 1 {:location "hackfest" :name "Virtual Moccamaster" :id 1})
  (create-coffee-level {:level 0.5 :time_stamp (to-sql-timestamp (new java.util.Date)) :coffee_pan_id 1})
  (list-coffee-level)
  (get-latest-coffee-level-for-pan 1))
