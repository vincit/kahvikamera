(ns kahviapina.handler
  (:require [compojure.core :refer [defroutes routes wrap-routes]]
            [kahviapina.layout :refer [error-page]]
            [kahviapina.routes.home :refer [home-routes]]
            [kahviapina.routes.services :refer [service-routes]]
            [kahviapina.middleware :as middleware]
            [kahviapina.db.core :as db]
            [compojure.route :as route]
            [kahviapina.routes.channels :as channels]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.3rd-party.rotor :as rotor]
            [selmer.parser :as parser]
            [environ.core :refer [env]]
            [kahviapina.config :refer [defaults]]
            [mount.core :as mount]))

(defn init
  "init will be called once when
   app is deployed as a servlet on
   an app server such as Tomcat
   put any initialization code here"
  []

  (timbre/merge-config!
    {:level     ((fnil keyword :info) (env :log-level))
     :appenders {:rotor (rotor/rotor-appender
                          {:path (or (env :log-path) "kahviapina.log")
                           :max-size (* 512 1024)
                           :backlog 10})}})
  (mount/start)
  ((:init defaults)))

(defn destroy
  "destroy will be called when your application
   shuts down, put any clean up code here"
  []
  (timbre/info "kahviapina is shutting down...")
  (mount/stop)
  (timbre/info "shutdown complete!"))

(def app-routes
  (routes
    (middleware/wrap-base service-routes)
    (wrap-routes #'home-routes middleware/wrap-csrf)
    (var channels/channels)
    (route/not-found
     (:body
        (error-page {:status 404
                     :title "page not found"})))))

(def app app-routes)
