(ns kahviapina.kahviapina.coffeestate
  (:require [clojure.core.matrix.stats :as math]
            ))

(def states (atom {}))

(def points (atom {}))


(defn add-point [{:keys [pan_id time_stamp coffee_level]}]
  (swap! points (fn [m]
                 (let [new-v (if-let [v (get m pan_id)]
                               (conj v [time_stamp coffee_level])
                               [[time_stamp coffee_level]])]
                   (assoc m pan_id new-v)))))

(defn pairwise [coll]
  "coll x -> coll [x x]"
  (let [c1 coll
        c2 (drop 1 coll)]
    (map vector c1 c2)))

(defn slope [[x1 y1] [x2 y2]]
  (let [dx (- x2 x1)
        dy (- y2 y1)]
    (if (or (= 0 dx) (= 0.0 dx))
      dx
      (/ dy dx))))

(defn to-slopes [sorted-levels]
  (map (fn [[p1 p2]] (slope p1 p2)) (pairwise sorted-levels)))

(defn mean-slope [levels]
  (let [sorted (sort-by first levels)
        slopes (to-slopes levels)]
    (math/mean slopes)))


(defn points-to-state [points]
  )

(defn points-to-states [points-map]
  )


(comment
  (mean-slope [[1 2] [2 3] [3 5] [4 8]]))
