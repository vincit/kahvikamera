(ns kahviapina.routes.helpers
  (:require [clojure.set :refer :all]))

(defn merge-with-set [m k v f]
  (merge-with f m {k #{v}}))

(defn add-to-map-set [k v m]
  (merge-with-set m k v union))

(defn remove-from-map-set [k v m]
  (merge-with-set m k v difference))
