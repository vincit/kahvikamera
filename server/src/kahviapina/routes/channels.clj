(ns kahviapina.routes.channels
  (:require
    [compojure.core :refer :all]
    [clojure.set :refer :all]
    [kahviapina.routes.helpers :refer :all]
    [taoensso.timbre :as timbre]
    [clojure.data.json :as json])
  (:use org.httpkit.server)
  (:import java.util.Date))

(def channels-by-pan-id (atom {}))


(defn for-each-channel-by-key [k m f]
  (if-let [vs (get m k)]
    (doseq [v vs]
      (if (open? v)
        (f v))))
    nil)

;============ COFFEE LEVEL ============

(defn coffee-level-channel [request]
  (let [pan-id (read-string (get-in request '(:params :pan-id)))]
    (with-channel request ch
                  (do
                    (swap! channels-by-pan-id (partial add-to-map-set pan-id ch))
                    (timbre/debug "Channel connected for pan " ch pan-id)
                    (on-close ch (fn [status]
                                   (timbre/debug "Channel disconnected for pan " ch pan-id)
                                   (swap! channels-by-pan-id (partial remove-from-map-set pan-id ch))))))))

(defn broadcast-level-for-pan [pan-id level]
  (for-each-channel-by-key pan-id @channels-by-pan-id
                           (fn [channel]
                             (let [msg (update level :time_stamp str)]
                               ;(timbre/debug (str "Sending message to pan " pan-id " " (:level level)))
                               ;(timbre/debug (str "Sending to channel " channel))
                               (send! channel (json/write-str msg)))
                             )))


;============ ROUTES ==================

(defroutes channels
   (context "/channels" []
            (GET "/coffee-pan-level/:pan-id" [] coffee-level-channel)))
