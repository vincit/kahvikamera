(ns kahviapina.routes.schemas
  (:require [schema.core :as s])
  (:import (java.sql Date)))

(s/defschema CoffeeLevel {(s/optional-key :id)         s/Int
                          :level                       s/Num
                          (s/optional-key :time_stamp) s/Str
                          :coffee_pan_id               s/Int})

(s/defschema CoffeePan {:id                            (s/maybe s/Int)
                        :name                          s/Str
                        (s/optional-key :latest_level) (s/maybe CoffeeLevel)
                        :coffee_maker_id               s/Int
                        :cap_x1                        s/Int
                        :cap_y1                        s/Int
                        :cap_x2                        s/Int
                        :cap_y2                        s/Int
                        })

(s/defschema CoffeeMaker {(s/optional-key :id)         s/Int
                          :name                        s/Str
                          :location                    s/Str
                          (s/optional-key :coffee_pan) [CoffeePan]
                          })
