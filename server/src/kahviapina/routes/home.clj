(ns kahviapina.routes.home
  (:require [kahviapina.layout :as layout]
            [compojure.core :refer :all]
            [ring.util.http-response :refer [ok]]
            [ring.util.response :as resp]
            [clojure.java.io :as io]))

(defn home-page []
  (resp/file-response "index.html" {:root "resources/public"}))

(defroutes home-routes
  (GET "/" []  (merge (home-page) {:headers {"Content-Type" "text/html"}}))
  (GET "/docs" [] (ok (-> "docs/docs.md" io/resource slurp))))
