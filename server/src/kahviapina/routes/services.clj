(ns kahviapina.routes.services
  (:require [ring.util.http-response :refer :all]
            [ring.util.response :as r]
            [compojure.api.sweet :refer :all]
            [schema.core :as s]
            [kahviapina.db.core :as db]
            [taoensso.timbre :as log]
            [kahviapina.routes.channels :as channels])
  (:use [kahviapina.routes.schemas])
  (:import (java.time Instant)
           (java.sql Timestamp)
           (java.util Date)
           (org.httpkit BytesInputStream)))


(defn timestamp-to-instant [^Timestamp timestamp]
  (.toInstant timestamp))

(defn update-timestamp [f]
  (fn [m] (update m :time_stamp f)))


(def latest-image-by-maker-id (atom {}))

(defn get-image [id]
  (if-let [bs (get @latest-image-by-maker-id id)]
    (new BytesInputStream bs (alength bs))))

(defapi service-routes
        (ring.swagger.ui/swagger-ui
          "/swagger-ui")
        ;JSON docs available at the /swagger.json route
        (swagger-docs
          {:info {:title "Sample api"}})
        (context* "/api" []
                  (context* "/coffeeMaker" {}
                            (GET* "/" []
                                  :return [CoffeeMaker]
                                  (ok (db/list-coffee-makers)))
                            (GET* "/:id" []
                                  :return CoffeeMaker
                                  :path-params [id :- Long]
                                  (ok (db/get-coffee-maker id)))
                            (POST* "/" []
                                   :return CoffeeMaker
                                   :body [maker (s/maybe CoffeeMaker)]
                                   (created (db/create-coffee-maker maker)))
                            (POST* "/:id/image"
                                   req
                                   :path-params [id :- Long]
                                   (swap! latest-image-by-maker-id assoc id (.bytes (:body req))))
                            (GET* "/:id/image"
                                  req
                                  :path-params [id :- Long]
                                  :query-params [time :- Long]
                                  (r/content-type (ok (get-image id)) "image/jpeg"))
                            (PUT* "/:id" []
                                  :return CoffeeMaker
                                  :path-params [id :- Long]
                                  :body [maker (s/maybe CoffeeMaker)]
                                  (db/update-coffee-maker id maker)
                                  ))
                  (context* "/coffeeLevel" {}
                            (POST* "/"
                                   req
                                   :return CoffeeLevel
                                   :body [level (s/maybe CoffeeLevel)]
                                   (let [level-with-ts (assoc level :time_stamp (Instant/now))]
                                     (log/debug "Received coffee level" (:level level) "from pan" (:coffee_pan_id level) "@" (:remote-addr req))
                                     (channels/broadcast-level-for-pan (get level-with-ts :coffee_pan_id) level-with-ts)
                                     (created (db/create-coffee-level level-with-ts))))
                            (GET* "/" []
                                  :return [CoffeeLevel]
                                  (ok (db/list-coffee-level))))

                  (context* "/coffeePan" {}
                            (POST* "/" []
                                   :return CoffeePan
                                   :body [pan (s/maybe CoffeePan)]
                                   (created (db/create-coffee-pan pan)))
                            (PUT* "/:id" []
                                  :return CoffeePan
                                  :path-params [id :- Long]
                                  :body [pan (s/maybe CoffeePan)]
                                  (ok (db/update-coffee-pan id pan)))
                            (GET* "/:coffee-pan-id/coffeeLevel/history" []
                                  :return [CoffeeLevel]
                                  :path-params [coffee-pan-id :- Long]
                                  (ok (->> (db/get-coffee-history coffee-pan-id)
                                           (map (update-timestamp timestamp-to-instant))
                                           (map (update-timestamp str)))))
                            (GET* "/:coffee-pan-id/coffeeLevel/latest" []
                                  :return [CoffeeLevel]
                                  :path-params [coffee-pan-id :- Long]
                                  (ok (db/get-latest-coffee-level-for-pan coffee-pan-id))))))
