(ns slack.slackpot 
  (:require [clj-http.client :as client])
  (:require [cheshire.core :refer :all]))

(defn message [username text]
  (def body
    {
      "channel" "#teamkahvi"
      "username" username
      "text" text
      "icon_emoji" ":coffee:"
    })
  (def url
    "https://hooks.slack.com/services/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")
  (= 
    (get
      (client/post url
      {
        :body (generate-string body)
        :content-type :json
      })
      :status)
    200))

