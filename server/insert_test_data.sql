BEGIN;
INSERT INTO coffee_maker (id, name, location) VALUES (1, 'Virtual Moccamaster', 'hackfest');
INSERT INTO coffee_pan (id, name, coffee_maker_id) VALUES (1, 'Vasen', 1);
INSERT INTO coffee_pan (id, name, coffee_maker_id) VALUES (2, 'Oikea', 1);
INSERT INTO coffee_level (time_stamp, level, coffee_pan_id) VALUES ('2015-11-18 22:23', 0.25, 1);
COMMIT;
