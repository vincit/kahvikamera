import numpy as np
import cv2
import cv2.cv as cv
import collections
import sys
import json
import urllib2
import os
import io
import requests
import copy
from analyzer import Analyzer
from analyzer_contrast import ContrastAnalyzer
from pprint import pprint
from threading import Timer
from datetime import timedelta, datetime
import time

server = ""

def postJSON(analyzer):
    global server
    print "Sending level to", server
    if server != "":
        data = {'level' : analyzer.getCoffeeLevel(), 'coffee_pan_id': analyzer.getPanId()}
        req = urllib2.Request(server +  '/api/coffeeLevel/')
        req.add_header('Content-Type', 'application/json')
        jsonData = json.dumps(data)
        print jsonData
        response = urllib2.urlopen(req, jsonData)
        response.read()
        response.close()

cap_area = []
analyzers = []
makerId = None

def on_mouse(event, x, y, flags, params):
    global cap_areas
    global cap_area
    if event == cv.CV_EVENT_LBUTTONDOWN:
        print 'Start Mouse Position: ' + str(x) + ', ' + str(y)
        sbox = (x, y)
        cap_area.append(sbox)

    elif event == cv.CV_EVENT_LBUTTONUP:
        print 'End Mouse Position: ' + str(x) + ', ' + str(y)
        ebox = (x, y)
        cap_area.append(ebox)
        #analyzers.append(Analyzer(cap_area, 0.1))
        cap_area = []


def sendLevel():
    global analyzers
    Timer(5.0, sendLevel).start()
    for (idx, analyzer) in enumerate(analyzers):
        try:
            postJSON(analyzer)
        except Exception, e:
            print e


def initGui():
    print "Initializing GUI"
    cv2.namedWindow('thresholdImg')
    cv2.namedWindow('frame')

    cv2.createTrackbar('P1', 'thresholdImg', 15, 255, nothing)

    cv.SetMouseCallback('frame', on_mouse, 0)

headless = True


def loadConfig():
    global analyzers
    global server
    global headless
    global makerId
    with open("config.json") as dataFile:
        data = json.load(dataFile)
        pprint(data)
        for pan in data['pans']:
            p1, p2 = pan['capture']
            capArea = [(p1[0], p1[1]), (p2[0], p2[1])]
            print capArea
            analyzers.append(Analyzer(capArea, pan['id']))
        server = data['server']
        headless = data['headless']
        makerId = data['makerId']
    
# BEGIN
argv = sys.argv
minute = 60 * 1000
hour = 60 * minute


cap = None 



def nothing(x):
    pass

loadConfig()


print "HEADLESS", headless
print "SERVER", server
print "ANALYZERS", analyzers


if not headless:
    initGui()
sendLevel()


latestFrame = None

def postImage():
    global makerId
    
    if latestFrame != None:

        img =  cv2.cvtColor(latestFrame, cv2.COLOR_BGR2RGB)
        r, buf = cv2.imencode(".jpg", img)
        headers = {'Content-Type': 'image/jpeg'}
        stream = io.BytesIO(buf)
        url = server + '/api/coffeeMaker/' + str(makerId) + '/image'
        print "Sending image to %s" % url
        requests.post(url, data=stream, headers=headers)

def postImages():
    try:
        postImage()
    except:
        print "Error posting image"
    Timer(1.0, postImages).start()

postImages()

def fetchConfig():
    global analyzers
    global makerId
    url = server + '/api/coffeeMaker/' + str(makerId)
    r = requests.get(url)
    result = r.json()

    for pan in result['coffee_pan']:
        for a in analyzers:
            if (pan['id'] == a.getPanId()):
                cap_area = [(pan['cap_x1'], pan['cap_y1']), (pan['cap_x2'], pan['cap_y2'])]
                print "Setting analyzer %s cap area to %s" % (a.getPanId(), cap_area)
                a.setCapArea(cap_area)

def pollConfig():
    try:
        fetchConfig()
    except: 
        print "Error fetching config"

    Timer(5.0, pollConfig).start()

pollConfig()

def handleFrame(frame):
    global analyzers
    global headless
    global latestFrame


    threshold = 35

    if not headless:
        key = cv2.waitKey(1) & 0xFF
        if ord("q") == key:
            return False
        threshold = cv2.getTrackbarPos('P1', 'thresholdImg')

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#    equalized = cv2.equalizeHist(gray)
    equalized = gray
    
    ret, thresholdImg = cv2.threshold(equalized, threshold, 255, cv2.THRESH_BINARY_INV)

    display = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

    for (idx, analyzer) in enumerate(analyzers):
        analyzer.analyzeFrame(display, thresholdImg)

    latestFrame = copy.deepcopy(display)

    if not headless:
        cv2.imshow('thresholdImg', thresholdImg)
        cv2.imshow('frame', display)
    return True



if ("raspberrypi" in argv[1]):
    from picamera.array import PiRGBArray
    from picamera import PiCamera    
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.brightness = 50
    camera.contrast = 70
#    camera.meter_mode = "spot"
#    time.sleep(2) #Wait for auto gain to settle
#    g = camera.awb_gains
#    camera.awb_mode = "off"
#    camera.shutter_speed = camera.exposure_speed
#    camera.awb_gains = g

    rawCapture = PiRGBArray(camera, size=(640, 480))
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port = True):
        cont = handleFrame(frame.array)
        if not cont:
            break
        rawCapture.truncate(0)
        
else:
    cap = cv2.VideoCapture(argv[1])
    cont = True
    seek =  1*hour + 13*minute #keitto
    #seek = 2 * hour + 40 * minute  # otto
    #seek = 0
    cap.set(cv.CV_CAP_PROP_POS_MSEC, seek)
    while (cap.isOpened() and cont):
        ret, frame = cap.read()        
        cont = handleFrame(frame)

    cap.release()
    cv2.destroyAllWindows()



