import numpy as np
import cv2
import cv2.cv as cv
import collections
import sys
import copy
class Analyzer:

    def __init__(self, capArea, panId):
        self.coffeLevelDeque = collections.deque(maxlen=700)
        self.capArea = capArea
        self.panId = panId

    def setCapArea(self, area):
        self.capArea = area

    def getPanId(self):
        return self.panId

    def capAreaHeight(self):
        (x1, y1), (x2, y2) = self.capArea
        return float(y2 - y1)

    def getCoffeeLevel(self):
        deque = self.coffeLevelDeque
        return sum(deque) / len(deque)

    def firCoffeeLevel(self, level):
        deque = self.coffeLevelDeque
        deque.append(level)
        return self.getCoffeeLevel()

    def rectangleArea(self, rect):
        x,y,w,h = rect
        return float(w)*float(h)
    
    def rectIntersectsTop(self, rect):
        x,y,w,h = rect
        (x1,y1), (x2,y2) = self.capArea
        return y1 == (y + y1 - 1)

    def rectIntersectsBottom(self, rect):
        x,y,w,h = rect
        (x1,y1), (x2,y2) = self.capArea
        return y2 == (y + y1 + h + 1)

    def drawRect(self, orig, rect, offset, color = (0,255,0)):
        x,y,w,h = rect 
        offsetX, offsetY = offset
        rectX, rectY = (x + offsetX, y + offsetY) 
        cv2.rectangle(orig, (rectX, rectY), (rectX+w,rectY+h), color, 2)

    def findNonCoffee(self, roi, orig, offset):
        #cv2.imshow('roi', roi)
        inverse = cv2.bitwise_not(roi)
        #cv2.imshow('inverse', inverse)
        contours, hier = cv2.findContours(inverse, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        boundingRects = map(lambda cnt: cv2.boundingRect(cnt), contours)
        boundingRects = filter(self.rectIntersectsTop, boundingRects)
        rect = None
        if boundingRects:
            rect = max(boundingRects, key=lambda c: self.rectangleArea(c))
        if rect:
            x,y,w,h = rect
            capHeight = self.capAreaHeight()
            level = (float((capHeight - h )) / capHeight) * 100.0
            self.drawRect(orig, rect, offset, color = (255,0,255))
            return level
        else:
            return 100.0

    def calcCoffeeContours(self, roi, orig, offset):
        height, width = roi.shape

        contours, hier = cv2.findContours(roi, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        (x1,y1), (x2,y2) = self.capArea
        
        boundingRects = map(lambda cnt: cv2.boundingRect(cnt), contours)
        boundingRects = filter(self.rectIntersectsBottom, boundingRects)
        # max area rect which touches bottom
        rect = None
        if boundingRects:
            rect = max(boundingRects, key=lambda c: self.rectangleArea(c))

        if rect:
            self.drawRect(orig, rect, offset)
            x, y, w, h = rect
            capHeight = self.capAreaHeight()
            level = (float(h)/capHeight) * 100.0
            return level
        else:
            return 0.0


    def analyzeFrame(self, frame, treshold):
        (x1,y1), (x2,y2) = self.capArea
        cv2.rectangle(frame, (x1, y1), (x2,y2), (0,0,255))
        roi = copy.copy(treshold)[y1:y2, x1:x2]
        offset = (x1, y1)
        level1 = self.findNonCoffee(roi, frame, offset)
        level2 = self.calcCoffeeContours(roi, frame, offset)
        level = min(level1, level2)
        #print "Level1", level1
        #print "Level2", level2
        #print "Level act", level
        self.firCoffeeLevel(level)
        cv2.putText(frame, "%.1f%s" % (self.getCoffeeLevel(), "%"), offset, cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0))

