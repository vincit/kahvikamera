import numpy as np
import cv2
import cv2.cv as cv
import collections
import sys
import copy
from analyzer import Analyzer
import time
import numpy.linalg as linalg

class ContrastAnalyzer(Analyzer):

    def calcContrast(self, pxBot, pxTop):
        (y,x,z) = pxTop.shape
        
        intesityDifference = 0
        for i in range(0, x):
            itop = linalg.norm(pxTop[0,i])
            ibot = linalg.norm(pxBot[0,i])
            if (ibot < itop):
                intesityDifference += abs(itop - ibot)
        return intesityDifference

    def maxContrast(self, frame):
        (cx1, cy1), (cx2, cy2) = self.capArea
        windowHeight = 40
        wy = cy2
        #print frame.shape
        #print(cy2, cy1)
        toCheck = range(cy1, cy2, windowHeight/4)

        dx = (cx2 - cx1)

        toPxTop = lambda y: frame[y:(y + 1), cx1:(dx + cx1)]
        toPxBot = lambda y: frame[(y + windowHeight -1):(y + windowHeight), cx1:(dx + cx1)]

        intensities = map(lambda y: self.calcContrast(toPxBot(y), toPxTop(y)), toCheck)

        dy, maxI = max(zip(toCheck, intensities), key=lambda (area,i): i)

        rect = (cx1,dy, (cx2 - cx1), windowHeight)
        print rect

        self.drawRect(frame, rect, (0,0))

    def analyzeFrame(self, frame, treshold):
        maxC = self.maxContrast(frame)